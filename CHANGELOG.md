# Changelog

All changelog and update vesioning.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2021-03-19

### Added

- new clicked event `onNextPageClicked(page)` and `onPrevPageClicked(page)`

## [1.0.1] - 2020-08-14

### Added

- new property `options`

### Changed
- css style, class

### Improvement
- improve ux

## [1.0.1-beta.0] - 2020-08-13

### Added

- new property `options`

### Changed
- css style, class

### Improvement
- improve ux

## [1.0.0] - 2020-03-18

### Added

- Add pagination component
- custom style
- custom slot

[1.0.1]: https://www.npmjs.com/package/@maqe-vue/pagination/v/1.0.1
[1.0.2]: https://www.npmjs.com/package/@maqe-vue/pagination/v/1.0.2
[1.0.1-beta.0]: https://www.npmjs.com/package/@maqe-vue/pagination/v/1.0.1-beta.0
[1.0.0]: https://www.npmjs.com/package/@maqe-vue/pagination/v/1.0.0
