# @maqe-vue/pagination
The Vue2 component for pagination

## Installation

### NPM
Install the npm package.
```
npm install @maqe-vue/pagination --save
```

### Register the component
**Global**
```
import Pagination from '@maqe-vue/pagination'

Vue.component('pagination', Pagination)
```

**Local**
```
<script>
import Pagination from '@maqe-vue/pagination'

export default {
	components: {
		Pagination
	}
}
</script>
```

### Included the style
Add the default style of component
```
import '@maqe-vue/pagination/dist/@maqe-vue/pagination.css'
```


## Usage

### Basic
```
<pagination
	:totalPage="10"
	:value="1"
	@onPageChange="handleChange"
/>
```

### Custom Previous and Next button
You can use `slot` for override original button

**Slot names**

| Name    | Description    |
| ------- | :------ |
| `prev`  | Previous button **default: "Prev"** |
| `next`  | Next button **default: "Next"** |

```
<pagination
	:totalPage="10"
	:value="1"
	@onPageChange="handleChange"
>
	<template v-slot:prev>
		<font-awesome-icon :icon="['far', 'angle-left']" />
	</template>
	<template v-slot:next>
		<font-awesome-icon :icon="['far', 'angle-right']" />
	</template>
</pagination>
```

### Custom Options
You can use property `options` to custom pagination type that depend on screen width

```
<pagination
	:totalPage="10"
	:value="1"
	:options="{
		type: 'full',
		breakpoints: {
			// large to small: that override smaller
			1024: { // screen width is lower 1024. so, this is for tablet
				type: 'compact'
			},
			580: { // screen width is lower 480. so, this is for mobile
				type: 'simple'
			}
		}
	}"
	@onPageChange="handleChange"
/>
```

### Custom style
You can add container class name as `containerClass` prop
```
<template>
	<pagination
		:totalPage="10"
		:value="1"
		containerClass="custom-pagination"
		@onPageChange="handleChange"
	>
		<template v-slot:prev>
			<font-awesome-icon :icon="['far', 'angle-left']" />
		</template>
		<template v-slot:next>
			<font-awesome-icon :icon="['far', 'angle-right']" />
		</template>
	</pagination>
</template>

<script>
export default {
	methods: {
		handleChange(page) => {
			console.log(page)
		}
	}
}
</script>

<style lang="scss">
.custom-pagination {
	--vmq-pagination-color: #ddd;
}
</style>
```

## Props and Computed

### Props

| Name              | Type     | Description    |
| ----------------- | :------- | :------------- |
| `totalPage`       | `Number` | Total count of pages. **default: 1** |
| `value`           | `Number` | The current page. **default: 1**  |
| `containerClass`  | `String` | Add container class name |
| `options`         | `Object` |
| `options.type` | `String` | `full` `compact` `simple` **default: `full`**
| `options.breakpoints` | `Object` | set `options.type` depend on screen width

### Computed

| Name |
| ----------------- |
| `firstPage`       |
| `lastPage`        |
| `isFirstPage`     |
| `isLastPage`      |
| `currentPage`     |