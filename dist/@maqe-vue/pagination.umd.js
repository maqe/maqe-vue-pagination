(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["@maqe-vue/pagination"] = factory();
	else
		root["@maqe-vue/pagination"] = factory();
})((typeof self !== 'undefined' ? self : this), function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "fb15");
/******/ })
/************************************************************************/
/******/ ({

/***/ "2aa4":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "5eaf":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Pagination_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2aa4");
/* harmony import */ var _node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Pagination_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_mini_css_extract_plugin_dist_loader_js_ref_8_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_8_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_8_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_8_oneOf_1_3_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Pagination_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* unused harmony reexport * */


/***/ }),

/***/ "8875":
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// addapted from the document.currentScript polyfill by Adam Miller
// MIT license
// source: https://github.com/amiller-gh/currentScript-polyfill

// added support for Firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1620505

(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
}(typeof self !== 'undefined' ? self : this, function () {
  function getCurrentScript () {
    var descriptor = Object.getOwnPropertyDescriptor(document, 'currentScript')
    // for chrome
    if (!descriptor && 'currentScript' in document && document.currentScript) {
      return document.currentScript
    }

    // for other browsers with native support for currentScript
    if (descriptor && descriptor.get !== getCurrentScript && document.currentScript) {
      return document.currentScript
    }
  
    // IE 8-10 support script readyState
    // IE 11+ & Firefox support stack trace
    try {
      throw new Error();
    }
    catch (err) {
      // Find the second match for the "at" string to get file src url from stack.
      var ieStackRegExp = /.*at [^(]*\((.*):(.+):(.+)\)$/ig,
        ffStackRegExp = /@([^@]*):(\d+):(\d+)\s*$/ig,
        stackDetails = ieStackRegExp.exec(err.stack) || ffStackRegExp.exec(err.stack),
        scriptLocation = (stackDetails && stackDetails[1]) || false,
        line = (stackDetails && stackDetails[2]) || false,
        currentLocation = document.location.href.replace(document.location.hash, ''),
        pageSource,
        inlineScriptSourceRegExp,
        inlineScriptSource,
        scripts = document.getElementsByTagName('script'); // Live NodeList collection
  
      if (scriptLocation === currentLocation) {
        pageSource = document.documentElement.outerHTML;
        inlineScriptSourceRegExp = new RegExp('(?:[^\\n]+?\\n){0,' + (line - 2) + '}[^<]*<script>([\\d\\D]*?)<\\/script>[\\d\\D]*', 'i');
        inlineScriptSource = pageSource.replace(inlineScriptSourceRegExp, '$1').trim();
      }
  
      for (var i = 0; i < scripts.length; i++) {
        // If ready state is interactive, return the script tag
        if (scripts[i].readyState === 'interactive') {
          return scripts[i];
        }
  
        // If src matches, return the script tag
        if (scripts[i].src === scriptLocation) {
          return scripts[i];
        }
  
        // If inline source matches, return the script tag
        if (
          scriptLocation === currentLocation &&
          scripts[i].innerHTML &&
          scripts[i].innerHTML.trim() === inlineScriptSource
        ) {
          return scripts[i];
        }
      }
  
      // If no match, return null
      return null;
    }
  };

  return getCurrentScript
}));


/***/ }),

/***/ "fb15":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/setPublicPath.js
// This file is imported into lib/wc client bundles.

if (typeof window !== 'undefined') {
  var currentScript = window.document.currentScript
  if (true) {
    var getCurrentScript = __webpack_require__("8875")
    currentScript = getCurrentScript()

    // for backward compatibility, because previously we directly included the polyfill
    if (!('currentScript' in document)) {
      Object.defineProperty(document, 'currentScript', { get: getCurrentScript })
    }
  }

  var src = currentScript && currentScript.src.match(/(.+\/)[^/]+\.js(\?.*)?$/)
  if (src) {
    __webpack_require__.p = src[1] // eslint-disable-line
  }
}

// Indicate to webpack that this file can be concatenated
/* harmony default export */ var setPublicPath = (null);

// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"7c485840-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Pagination.vue?vue&type=template&id=5f4e7b1e&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('section',{class:['vmq-pagination-container' ].concat( _vm.wrapperClass)},[_c('ul',{staticClass:"vmq-pagination"},[_c('li',[_c('button',{staticClass:"vmq-pagination-prev",attrs:{"disabled":_vm.isFirstPage},on:{"click":function($event){$event.preventDefault();return _vm.prevPage($event)}}},[_c('div',{staticClass:"text"},[_vm._t("prev",[_c('img',{attrs:{"src":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAQAAAD9CzEMAAAABGdBTUEAALGPC/xhBQAAACBjSFJN\n\t\t\t\t\t\t\tAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElN\n\t\t\t\t\t\t\tRQfkBBQKGg4x+Mc2AAABjElEQVRYw7XXO0/CUBjG8b9tKCwCizcwUSOYaIy7q4mDKzXR7+i38BOo\n\t\t\t\t\t\t\ti4ubi5EetGpi1CBuROE9p+dmt74nPD+e3ijwz1tqXG3zzTgMWNCutDhmgy+uuWQSv0GLM5aBlA5t\n\t\t\t\t\t\t\t7mIDTc5ZnO4thRAycMT6n/0AIhVnJyQzM29CAiZs0pybehLyIXplV7i+vAgZKFH04xC6y3SEYicG\n\t\t\t\t\t\t\tob+TIxGmR4WKQZifRYpR6LlIK9aDiSoAFE8hRDUARQhhA0DBMz0/wg4IIGwBKCh9CHsAhpT0hbmR\n\t\t\t\t\t\t\tcAG8CDcAhrzQcyFcAROhUDEAGPLBljDPuJ0fJpVx81uDPXFeSEN3oMEpK8L8k6sYgD7+gjIcqBvi\n\t\t\t\t\t\t\tH+SPuAAe8aZ3Uyl+1TXevkGd3CfetkGdnDWfeLsGmX+8DRAUX32IMnI6/vFVDTIGYfHmBhkDumHx\n\t\t\t\t\t\t\tpga1GPH6BjXyGPG6BpG+vR44nPmP5h0vAyn7seJlYMx7rHjdb/KY7TjxOuCR5NdZCIjXv1XcT4mg\n\t\t\t\t\t\t\tePOd3OWAkhve/OPhB7oEhrCYVbz9AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIwLTA0LTIwVDEwOjI2\n\t\t\t\t\t\t\tOjE0KzAzOjAw0+VYRAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0wNC0yMFQxMDoyNjoxNCswMzow\n\t\t\t\t\t\t\tMKK44PgAAAAASUVORK5CYII="}}),(_vm.showText)?_c('span',[_vm._v(" Prev ")]):_vm._e()])],2)])]),_vm._l((_vm.filterPaginators),function(paginator,index){return _c('li',{key:index,staticClass:"vmq-pagination-button"},[_c('button',{class:paginator.btnClass,on:{"click":function($event){$event.preventDefault();return _vm.setPage(paginator.value)}}},[_c('span',{staticClass:"text"},[_vm._v(" "+_vm._s(paginator.value)+" ")])])])}),_c('li',[_c('button',{staticClass:"vmq-pagination-next",attrs:{"disabled":_vm.isLastPage},on:{"click":function($event){$event.preventDefault();return _vm.nextPage($event)}}},[_c('div',{staticClass:"text"},[_vm._t("next",[(_vm.showText)?_c('span',[_vm._v(" Next ")]):_vm._e(),_c('img',{attrs:{"src":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAQAAAD9CzEMAAAABGdBTUEAALGPC/xhBQAAACBjSFJN\n\t\t\t\t\t\tAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElN\n\t\t\t\t\t\tRQfkBBQKGg4x+Mc2AAABjElEQVRYw7XXO0/CUBjG8b9tKCwCizcwUSOYaIy7q4mDKzXR7+i38BOo\n\t\t\t\t\t\ti4ubi5EetGpi1CBuROE9p+dmt74nPD+e3ijwz1tqXG3zzTgMWNCutDhmgy+uuWQSv0GLM5aBlA5t\n\t\t\t\t\t\t7mIDTc5ZnO4thRAycMT6n/0AIhVnJyQzM29CAiZs0pybehLyIXplV7i+vAgZKFH04xC6y3SEYicG\n\t\t\t\t\t\tob+TIxGmR4WKQZifRYpR6LlIK9aDiSoAFE8hRDUARQhhA0DBMz0/wg4IIGwBKCh9CHsAhpT0hbmR\n\t\t\t\t\t\tcAG8CDcAhrzQcyFcAROhUDEAGPLBljDPuJ0fJpVx81uDPXFeSEN3oMEpK8L8k6sYgD7+gjIcqBvi\n\t\t\t\t\t\tH+SPuAAe8aZ3Uyl+1TXevkGd3CfetkGdnDWfeLsGmX+8DRAUX32IMnI6/vFVDTIGYfHmBhkDumHx\n\t\t\t\t\t\tpga1GPH6BjXyGPG6BpG+vR44nPmP5h0vAyn7seJlYMx7rHjdb/KY7TjxOuCR5NdZCIjXv1XcT4mg\n\t\t\t\t\t\tePOd3OWAkhve/OPhB7oEhrCYVbz9AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIwLTA0LTIwVDEwOjI2\n\t\t\t\t\t\tOjE0KzAzOjAw0+VYRAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0wNC0yMFQxMDoyNjoxNCswMzow\n\t\t\t\t\t\tMKK44PgAAAAASUVORK5CYII="}})])],2)])])],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./src/components/Pagination.vue?vue&type=template&id=5f4e7b1e&

// CONCATENATED MODULE: ./src/assets/js/pagination.js
var getPaginations = function getPaginations(_ref) {
  var _ref$firstPage = _ref.firstPage,
      firstPage = _ref$firstPage === void 0 ? 1 : _ref$firstPage,
      lastPage = _ref.lastPage,
      currentPage = _ref.currentPage,
      _ref$offsetPage = _ref.offsetPage,
      offsetPage = _ref$offsetPage === void 0 ? 2 : _ref$offsetPage;
  var paginators = [];
  var expectLength = offsetPage * 2 + 2;
  var state = {
    nearFirst: false,
    middle: false,
    nearLast: false
  };

  if (lastPage < offsetPage * 2 + 1) {
    for (var i = firstPage; i < lastPage + 1; i += 1) {
      paginators.push({
        value: i,
        enable: true
      });
    }
  } else if (currentPage - firstPage < offsetPage + 2) {
    // if currentPage near firstPage
    state.nearFirst = true;

    for (var _i = firstPage; _i < Math.max(offsetPage * 2, currentPage + offsetPage); _i += 1) {
      paginators.push({
        value: _i,
        enable: true
      });
    }

    paginators.push({
      value: "...",
      enable: false
    });
    paginators.push({
      value: lastPage,
      enable: true
    });
  } else if (lastPage - currentPage < offsetPage + 2) {
    // if currentPage near lastPage
    state.nearLast = true;
    paginators.push({
      value: firstPage,
      enable: true
    });
    paginators.push({
      value: "...",
      enable: false
    });

    for (var _i2 = Math.min(lastPage - offsetPage * 2 + 2, currentPage - offsetPage); _i2 < lastPage + 1; _i2 += 1) {
      paginators.push({
        value: _i2,
        enable: true
      });
    }
  } else {
    // if currentPage in the middle
    state.middle = true;
    paginators.push({
      value: firstPage,
      enable: true
    });
    paginators.push({
      value: "...",
      enable: false
    });

    for (var _i3 = currentPage - offsetPage + 1; _i3 < currentPage + offsetPage; _i3 += 1) {
      paginators.push({
        value: _i3,
        enable: true
      });
    }

    paginators.push({
      value: "...",
      enable: false
    });
    paginators.push({
      value: lastPage,
      enable: true
    });
  }

  if (lastPage < expectLength) {
    paginators.length = 0;

    for (var _i4 = 0; _i4 < lastPage; _i4++) {
      paginators.push({
        value: _i4 + 1,
        enable: true
      });
    }
  } else if (state.nearFirst) {
    if (paginators.length < expectLength) {
      var dottedAtIndex = paginators.findIndex(function (page) {
        return page.value === '...';
      });

      if (dottedAtIndex !== -1) {
        paginators.splice(dottedAtIndex, 0, {
          value: paginators[dottedAtIndex - 1].value + 1,
          enable: true
        });
      }
    }

    var currentPageAtIndex = paginators.findIndex(function (page) {
      return page.value === currentPage;
    });
    var prevPageCount = paginators.filter(function (page, index) {
      return page.value !== firstPage && index < currentPageAtIndex;
    }).length;

    if (prevPageCount > offsetPage - 1) {
      var _paginators$replaceIn;

      var replaceIndex = currentPageAtIndex - offsetPage;
      var expectDotted = ((_paginators$replaceIn = paginators[replaceIndex]) === null || _paginators$replaceIn === void 0 ? void 0 : _paginators$replaceIn.value) === '...';

      if (!expectDotted) {
        paginators[replaceIndex].value = "...";
        paginators[replaceIndex].enable = false;
      }
    }
  } else if (state.nearLast) {
    var _paginators;

    var startedAtPage = currentPage - (offsetPage - 1);
    var stopAtPage = currentPage + (offsetPage - 1);

    var _removeIndexs = paginators.reduce(function (indexs, _ref2, index) {
      var value = _ref2.value;

      if ((value < startedAtPage || value > stopAtPage) && value !== firstPage && value !== lastPage && value !== '...') {
        indexs.push(index);
      }

      return indexs;
    }, []);

    while (_removeIndexs.length) {
      paginators.splice(_removeIndexs.pop(), 1);
    }

    if (((_paginators = paginators[paginators.length - 2]) === null || _paginators === void 0 ? void 0 : _paginators.value) + 1 < lastPage) {
      paginators.splice(paginators.length - 1, 0, {
        value: "...",
        enable: false
      });
    } else if (paginators.length < expectLength) {
      var _paginators2;

      var pageNumberCountNeededToAdd = expectLength - paginators.length;

      var _dottedAtIndex = paginators.findIndex(function (page) {
        return page.value === '...';
      });

      var afterDottedNumber = (_paginators2 = paginators[_dottedAtIndex + 1]) === null || _paginators2 === void 0 ? void 0 : _paginators2.value;

      if (afterDottedNumber !== undefined) {
        for (var _i5 = 0; _i5 < pageNumberCountNeededToAdd; _i5++) {
          paginators.splice(_dottedAtIndex + 1, 0, {
            value: afterDottedNumber - 1 - _i5,
            enable: false
          });
        }
      }
    }
  }

  var removeIndexs = [];
  paginators.forEach(function (page, index) {
    if (page.value === "...") {
      var _paginators3, _paginators4;

      var next = (_paginators3 = paginators[index + 1]) === null || _paginators3 === void 0 ? void 0 : _paginators3.value;
      var prev = (_paginators4 = paginators[index - 1]) === null || _paginators4 === void 0 ? void 0 : _paginators4.value;

      if (Number.isInteger(next) && Number.isInteger(prev)) {
        if (next - prev === 1) {
          removeIndexs.push(index);
        }
      }
    }
  });

  while (removeIndexs.length) {
    paginators.splice(removeIndexs.pop(), 1);
  }

  return paginators;
};
/* harmony default export */ var pagination = ({
  getPaginations: getPaginations
});
// CONCATENATED MODULE: ./src/assets/js/enum.js
var TYPE = Object.freeze({
  ENUM: {
    FULL: "full",
    COMPACT: "compact",
    SIMPLE: "simple"
  },

  get values() {
    return Object.values(this.ENUM);
  }

});
var PAGE_STATE = Object.freeze({
  ENUM: {
    FIRST: "FIRST",
    MIDDLE: "MIDDLE",
    LAST: "LAST"
  },

  get values() {
    return Object.values(this.ENUM);
  }

});
// CONCATENATED MODULE: ./src/assets/js/utils.js

var utils_isValidType = function isValidType(type) {
  return TYPE.values.includes(type);
};
var isNumber = function isNumber(n) {
  return Number.isInteger(n * 1);
};
// CONCATENATED MODULE: ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Pagination.vue?vue&type=script&lang=js&
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ var Paginationvue_type_script_lang_js_ = ({
  name: "Pagination",
  props: {
    totalPage: {
      type: Number,
      default: 1
    },
    value: {
      type: Number,
      default: 1
    },
    containerClass: {
      type: String,
      default: ""
    },
    options: {
      type: Object,
      default: function _default() {
        return {};
      },
      validator: function validator() {
        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        if (options.type && !utils_isValidType(options.type)) {
          return false;
        }

        if (_typeof(options.breakpoints) === "object" && !Array.isArray(options.breakpoints)) {
          var isValid = function isValid(breakpoint) {
            return [isNumber(breakpoint), utils_isValidType(options.breakpoints[breakpoint].type) // add new validation here
            ].every(function (v) {
              return v;
            });
          };

          return Object.keys(options.breakpoints).map(isValid).every(function (v) {
            return v;
          });
        }

        return true;
      }
    }
  },
  data: function data() {
    return {
      type: null,
      defaultOption: {
        type: TYPE.ENUM.FULL,
        breakpoints: {}
      }
    };
  },
  computed: {
    firstPage: function firstPage() {
      return 1;
    },
    lastPage: function lastPage() {
      return this.totalPage;
    },
    isFirstPage: function isFirstPage() {
      return this.currentPage === this.firstPage;
    },
    isLastPage: function isLastPage() {
      return this.currentPage === this.lastPage;
    },
    currentPage: function currentPage() {
      return this.value;
    },
    paginators: function paginators() {
      return getPaginations({
        firstPage: this.firstPage,
        lastPage: this.lastPage,
        currentPage: this.currentPage,
        offsetPage: this.offsetPage
      });
    },
    wrapperClass: function wrapperClass() {
      return [this.containerClass, this.typeClass, this.pageStateClass];
    },
    typeClass: function typeClass() {
      return "vmq-pagination-".concat(this.type);
    },
    offsetPage: function offsetPage() {
      var _mappingOffsetPage;

      var defaultOffsetPage = 1;
      var mappingOffsetPage = (_mappingOffsetPage = {}, _defineProperty(_mappingOffsetPage, TYPE.ENUM.COMPACT, 2), _defineProperty(_mappingOffsetPage, TYPE.ENUM.FULL, 3), _mappingOffsetPage);
      return mappingOffsetPage[this.type] || defaultOffsetPage;
    },
    pageStateClass: function pageStateClass() {
      var _PAGE_STATE$ENUM = PAGE_STATE.ENUM,
          FIRST = _PAGE_STATE$ENUM.FIRST,
          MIDDLE = _PAGE_STATE$ENUM.MIDDLE,
          LAST = _PAGE_STATE$ENUM.LAST;
      var pageState = this.pageState;

      if (pageState === FIRST) {
        return "vmq-page-first";
      } else if (pageState === MIDDLE) {
        return "vmq-page-middle";
      } else if (pageState === LAST) {
        return "vmq-page-last";
      }

      return null;
    },
    filterPaginators: function filterPaginators() {
      var _this = this;

      var isSimpleType = this.type === TYPE.ENUM.SIMPLE;
      var isLastPage = this.currentPage === this.lastPage; // this function use to filter paginator

      var isValidConditionType = function isValidConditionType(page) {
        // filter only `simple` type
        // such as: we have 50 total page and current page is 1
        // it should return two array only, current and last page.
        if (isSimpleType) {
          var isCurrentOrLast = page.value === _this.currentPage || page.value === _this.lastPage;
          return isCurrentOrLast;
        }

        return true;
      };

      var mapStatus = function mapStatus(page) {
        var isDottedValue = page.value === "...";
        var enable = true;
        var clickable = true;

        if (isSimpleType || isDottedValue) {
          clickable = false;
        }

        return _objectSpread(_objectSpread({}, page), {}, {
          enable: enable,
          clickable: clickable
        });
      };

      var applyButtonClass = function applyButtonClass(page) {
        return _objectSpread(_objectSpread({}, page), {}, {
          btnClass: ['vmq-pagination-link', {
            'vmq-pagination-current-page': _this.currentPage === page.value,
            'disabled': !page.enable,
            'unclickable': !page.clickable,
            'is-dotted': page.value === '...'
          }, page.btnClass]
        });
      };

      var results = this.paginators.filter(isValidConditionType).map(mapStatus).map(applyButtonClass); // for `simple` page, push `Of` to the middle of array.
      // It will be like this `1 of 50`

      if (isSimpleType && !isLastPage) {
        results.splice(1, 0, {
          value: "Of",
          enable: true,
          clickable: false,
          btnClass: 'vmq-pagination-of unclickable'
        });
      }

      return results;
    },
    mergeOptions: function mergeOptions() {
      return _objectSpread(_objectSpread({}, this.defaultOption), this.options);
    },
    isSetBreakpoints: function isSetBreakpoints() {
      return !!Object.keys(this.mergeOptions.breakpoints).length;
    },
    showText: function showText() {
      return this.type === TYPE.ENUM.FULL;
    },
    pageState: function pageState() {
      var found2Dotted = this.filterPaginators.filter(function (page) {
        return page.value === "...";
      }).length === 2;
      var foundDottedAtIndex = this.filterPaginators.findIndex(function (page) {
        return page.value === "...";
      });

      if (found2Dotted) {
        return PAGE_STATE.ENUM.MIDDLE;
      } else if (foundDottedAtIndex > 1) {
        return PAGE_STATE.ENUM.FIRST;
      } else if (foundDottedAtIndex === 1) {
        return PAGE_STATE.ENUM.LAST;
      }

      return null;
    }
  },
  mounted: function mounted() {
    this.type = window.innerWidth ? this.getPaginationType(window.innerWidth) : this.mergeOptions.type;
    window.addEventListener('resize', this.handleWindowResize);
  },
  methods: {
    nextPage: function nextPage() {
      var newPage = this.value + 1;
      this.$emit("onNextPageClicked", newPage);
      this.$emit("onPageChange", newPage);
    },
    prevPage: function prevPage() {
      var newPage = this.value - 1;
      this.$emit("onPrevPageClicked", newPage);
      this.$emit("onPageChange", newPage);
    },
    setPage: function setPage(n) {
      this.$emit("onPageChange", n);
    },
    // this function use to get type, depends on screen width and breakpoints
    // if match any then return `type`, if not return null
    getMatchedType: function getMatchedType(screenWidth, breakpoints) {
      var matchType = null;

      for (var _i = 0, _Object$keys = Object.keys(breakpoints); _i < _Object$keys.length; _i++) {
        var key = _Object$keys[_i];
        // as specific breakpoints `key` number.

        /**
        	{
        		480: {
        			type: "simple"
        		}
        	}
        **/
        var breakpointWidth = key;

        if (screenWidth < breakpointWidth) {
          matchType = breakpoints[key].type;
          break;
        }
      }

      return matchType;
    },
    getPaginationType: function getPaginationType(screenWidth) {
      var breakpoints = this.mergeOptions.breakpoints;
      var matchedType = this.getMatchedType(screenWidth, breakpoints);
      return matchedType || this.mergeOptions.type;
    },
    handleWindowResize: function handleWindowResize(e) {
      if (!this.isSetBreakpoints) {
        return;
      }

      var screenWidth = e.currentTarget.innerWidth; // if match any breakpoints then set as specific, if not set as default.

      this.type = this.getPaginationType(screenWidth);
    }
  },
  destroyed: function destroyed() {
    window.removeEventListener('resize', this.handleWindowResize);
  }
});
// CONCATENATED MODULE: ./src/components/Pagination.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_Paginationvue_type_script_lang_js_ = (Paginationvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./src/components/Pagination.vue?vue&type=style&index=0&lang=scss&
var Paginationvue_type_style_index_0_lang_scss_ = __webpack_require__("5eaf");

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}

// CONCATENATED MODULE: ./src/components/Pagination.vue






/* normalize component */

var component = normalizeComponent(
  components_Paginationvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* harmony default export */ var Pagination = (component.exports);
// CONCATENATED MODULE: ./src/index.js

/* harmony default export */ var src_0 = (Pagination);
// CONCATENATED MODULE: ./node_modules/@vue/cli-service/lib/commands/build/entry-lib.js


/* harmony default export */ var entry_lib = __webpack_exports__["default"] = (src_0);



/***/ })

/******/ });
});
//# sourceMappingURL=pagination.umd.js.map