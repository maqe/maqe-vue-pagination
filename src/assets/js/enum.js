export const TYPE = Object.freeze({
	ENUM: {
		FULL: "full",
		COMPACT: "compact",
		SIMPLE: "simple",
	},
	get values() {
		return Object.values(this.ENUM)
	}
});

export const PAGE_STATE = Object.freeze({
	ENUM: {
		FIRST: "FIRST",
		MIDDLE: "MIDDLE",
		LAST: "LAST",
	},
	get values() {
		return Object.values(this.ENUM)
	}
});