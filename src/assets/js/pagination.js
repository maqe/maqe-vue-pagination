export const getPaginations = ({ firstPage = 1, lastPage, currentPage, offsetPage = 2 }) => {
	const paginators = [];
	const expectLength = offsetPage * 2 + 2;
	const state = {
		nearFirst: false,
		middle: false,
		nearLast: false
	}

	if (lastPage < (offsetPage * 2) + 1) {
		for (let i = firstPage; i < lastPage + 1; i += 1) {
			paginators.push({
				value: i,
				enable: true
			});
		}
	} else if (currentPage - firstPage < offsetPage + 2) {
		// if currentPage near firstPage
		state.nearFirst = true

		for (
			let i = firstPage;
			i < Math.max(offsetPage * 2, currentPage + offsetPage);
			i += 1
		) {
			paginators.push({
				value: i,
				enable: true
			});
		}

		paginators.push({
			value: "...",
			enable: false
		});

		paginators.push({
			value: lastPage,
			enable: true
		});
	} else if (lastPage - currentPage < offsetPage + 2) {
		// if currentPage near lastPage
		state.nearLast = true

		paginators.push({
			value: firstPage,
			enable: true
		});

		paginators.push({
			value: "...",
			enable: false
		});

		for (
			let i = Math.min(
				(lastPage - (offsetPage * 2)) + 2,
				currentPage - offsetPage
			); i < lastPage + 1; i += 1) {
			paginators.push({
				value: i,
				enable: true
			});
		}
	} else {
		// if currentPage in the middle
		state.middle = true

		paginators.push({
			value: firstPage,
			enable: true
		});

		paginators.push({
			value: "...",
			enable: false
		});

		for (
			let i = (currentPage - offsetPage) + 1;
			i < currentPage + offsetPage; i += 1) {
			paginators.push({
				value: i,
				enable: true
			});
		}

		paginators.push({
			value: "...",
			enable: false
		});

		paginators.push({
			value: lastPage,
			enable: true
		});
	}

	if(lastPage < expectLength) {
		paginators.length = 0

		for(let i = 0; i < lastPage; i++) {
			paginators.push({
				value: i + 1,
				enable: true
			})
		}
	} else if(state.nearFirst) {
		if(paginators.length < expectLength) {
			const dottedAtIndex = paginators.findIndex((page) => page.value === '...')
			
			if(dottedAtIndex !== -1) {
				paginators.splice(dottedAtIndex, 0, {
					value: paginators[dottedAtIndex-1].value + 1,
					enable: true
				})
			}			
		}

		const currentPageAtIndex = paginators.findIndex((page) => page.value === currentPage)
		const prevPageCount = paginators.filter((page, index) => page.value !== firstPage && index < currentPageAtIndex).length

		if(prevPageCount > (offsetPage - 1)) {
			const replaceIndex = currentPageAtIndex - (offsetPage)
			const expectDotted = paginators[replaceIndex]?.value === '...'

			if(!expectDotted) {
				paginators[replaceIndex].value = "...";
				paginators[replaceIndex].enable = false;
			}
		}
	} else if(state.nearLast) {
		const startedAtPage = currentPage - (offsetPage - 1)
		const stopAtPage = currentPage + (offsetPage - 1);
		let removeIndexs = paginators.reduce((indexs, { value }, index) => {
			if((value < startedAtPage || value > stopAtPage) && value !== firstPage && value !== lastPage && value !== '...') {
				indexs.push(index)
			}
			return indexs;
		}, []);

		while(removeIndexs.length) {
			paginators.splice(removeIndexs.pop(), 1);
		}

		if(paginators[paginators.length-2]?.value + 1 < lastPage) {
			paginators.splice(paginators.length-1, 0, {
				value: "...",
				enable: false
			})
		} else if(paginators.length < expectLength) {
			const pageNumberCountNeededToAdd = expectLength - paginators.length
			const dottedAtIndex = paginators.findIndex((page) => page.value === '...')
			const afterDottedNumber = paginators[dottedAtIndex+1]?.value
			
			if(afterDottedNumber !== undefined) {
				for(let i = 0; i < pageNumberCountNeededToAdd; i++) {
					paginators.splice(dottedAtIndex + 1, 0, {
						value: afterDottedNumber - 1 - i,
						enable: false
					})
				}
			}
		}
	}

	let removeIndexs = []

	paginators.forEach((page, index) => {
		if(page.value === "...") {
			const next = paginators[index+1]?.value;
			const prev = paginators[index-1]?.value;
			
			if(Number.isInteger(next) && Number.isInteger(prev)) {
				if(next - prev === 1) {
					removeIndexs.push(index)
				}
			}
		}
	})

	while(removeIndexs.length) {
		paginators.splice(removeIndexs.pop(), 1);
	}

	return paginators;
};

export default {
	getPaginations
};
