import { TYPE } from "./enum";

export const isValidType = (type) => TYPE.values.includes(type);

export const isNumber = (n) => Number.isInteger(n * 1);